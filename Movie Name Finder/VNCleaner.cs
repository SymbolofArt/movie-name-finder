﻿using System.Text.RegularExpressions;

namespace Movie_Name_Finder
{
    public class VNCleaner
    {
       /// <summary>
       /// Get the "Movie" name of the filename. Removing unused data. If filenames title can't be detected it will return the inputed filename.
       /// </summary>
       /// <param name="filename">Filename to convert to "movie name"</param>
       /// <returns></returns>
        public string GetVideoName(string filename)
        {
            string videoName = filename;

            MovieOrSerieDetector movieDetect = new MovieOrSerieDetector();

            //Check if its a movie or serie and either go to get movie name or serie name.
            if (movieDetect.isMovie(videoName) == true)
                videoName = GetMovieName(videoName);
            else
                videoName = GetSerieName(videoName);

            //return the "Converted" name.
            return videoName;
        }

        private string GetMovieName(string filename)
        {
            string movieName = filename;
            movieName = replaceSpaceDot(movieName);
            movieName = CleanUpBrackers(movieName);
            movieName = GetMovieNameClean(movieName);
            movieName = ReplaceDots(movieName);
            if (movieName == string.Empty)
                movieName = filename;
            return movieName;
        }
        
        /// <summary>
        /// Replace space with dots. For exempel "The Exemple Movie" would be "The.Exemple.Movie"
        /// </summary>
        /// <param name="s">String to replace space with dots.</param>
        /// <returns></returns>
        private string replaceSpaceDot(string s)
        {
            string answer = "";

            answer = s.Replace(" ", ".");

            return answer;

        }

        private string GetSerieName(string filename)
        {
            string serieName = filename;
            serieName = CleanUpBrackers(serieName);
            string se = getSE(serieName);

            serieName = GetMovieNameClean(serieName);
            serieName = ReplaceDots(serieName);
            serieName += " - " + se;
            return serieName;
        }



        private string getSE(string serieName)
        {
            string SE = "S00E00";


            Regex roxy = new Regex(@"(S(\d{1,2})E(\d{2})|s(\d{1,2})e(\d{2}))");
            MatchCollection matches = roxy.Matches(serieName);

            foreach (Match match in matches)
                SE = match.Groups[1].ToString().Trim();

            return SE;
        }

        private string CleanUpBrackers(string s)
        {
            //uses regex to remove brackets information.
            string answer = Regex.Replace(s, @"/\[.*?\]|\(.*?\)/", ".");

            return answer;
        }

        private string GetMovieNameClean(string s)
        {

            string answer = "";

            Regex roxy = new Regex(@"(.*?)\.(\d{4})?\.?(S(\d{1,2})E(\d{2})|s(\d{1,2})e(\d{2}))?\.(.*)");
            MatchCollection matches = roxy.Matches(s);

            foreach (Match match in matches)
                answer = match.Groups[1].ToString().Trim();



            if (answer == "")
                answer = s;
            return answer;
        }

        /// <summary>
        /// Replace dots with space. For exemple "The.Exemple.Movie" would be "The Exemple Movie"
        /// </summary>
        /// <param name="s">String to replace dots with space.</param>
        /// <returns></returns>
        private string ReplaceDots(string s)
        {
            string answer = "";

            answer = s.Replace(".", " ");

            return answer;
        }
    }
}
