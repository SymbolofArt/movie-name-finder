﻿using System.Text.RegularExpressions;

namespace Movie_Name_Finder
{
    class MovieOrSerieDetector
    {
        // String to detect if filename has SxxExx or sxxexx in its name, where x is a number,
        // tex S1E01 or s13e24 --- The.Legend.of.Everything.s03e03 = serie, The.Legend = movie
       
        //Regex expression used for detecting.
        string regexExp = @"(S(\d{1,2})E(\d{2})|s(\d{1,2})e(\d{2}))";

        /// <summary>
        /// Returns true if filename is detected as being a "movie". Other wise return false.
        /// </summary>
        /// <param name="filename">Filename to check.</param>
        /// <returns></returns>
        public bool isMovie(string filename)
        {
            return !Regex.IsMatch(filename, regexExp);
        }

        /// <summary>
        /// Retruns true if filename is detected as being a "serie". Other wise return false.
        /// </summary>
        /// <param name="filename">Filename to check</param>
        /// <returns></returns>
        public bool isSerie(string filename)
        {
            return Regex.IsMatch(filename, regexExp);
        }
    }
}
