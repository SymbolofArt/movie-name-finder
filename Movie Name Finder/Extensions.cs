﻿namespace Movie_Name_Finder
{
    class Extensions
    {
        //Subtitles formats.
        string[] subtitle_exts = new string[] { "srt", "idx", "sub", "ssa", "ass" };

        //Info text files
        string[] info_exts = new string[] { "nfo" };

        //All video file names avaible.
        string[] video_exts = new string[] {".3g2", ".3gp", ".3gp2", ".asf", ".avi", ".divx", ".flv", ".m4v", ".mk2",
             "mka", ".mkv", ".mov", ".mp4", ".mp4a", ".mpeg", ".mpg", ".ogg", ".ogm",
             "ogv", ".qt", ".ra", ".ram", ".rm", ".ts", ".wav", ".webm", ".wma", ".wmv",
             ".iso"};

        public string[] VideoExts
        {
            get { return video_exts; }
        }
    }
}
